package com.ndt_lab.store.mapper;

import com.ndtLab.store.dto.ProductResponseDTO;
import com.ndtLab.store.entity.Manufacturer;
import com.ndtLab.store.entity.Product;
import com.ndtLab.store.entity.ProductHistory;
import com.ndtLab.store.entity.Provider;
import com.ndtLab.store.mapper.ProductMapper;
import org.junit.jupiter.api.Test;
import org.mapstruct.factory.Mappers;

import static org.assertj.core.api.AssertionsForClassTypes.assertThat;

import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.time.Month;
import java.util.*;

public class ProductMapperTest {

    private ProductMapper INSTANCE = Mappers.getMapper( ProductMapper.class );

    @Test
    public void shouldMapProductToDTO() {


        Product product = new Product(1L, "123ABC", new ArrayList<>());

        Manufacturer manufacturer = new Manufacturer(1L, "test manufacturer");
        Provider provider = new Provider(1L, "test provider");
        BigDecimal price = BigDecimal.valueOf(100);
        BigDecimal discount = BigDecimal.valueOf(10);

        ProductHistory productHistory = new ProductHistory(1L, product, "test name",
                "test method", price, discount, false, manufacturer, provider,
                "test descriprion", LocalDateTime.of(2021, Month.MAY, 9, 11, 6, 22),
                LocalDateTime.of(2500, Month.JANUARY, 1, 0, 0, 0), 1,
                LocalDateTime.of(2021, Month.MAY, 9, 11, 6, 22),
                1, LocalDateTime.of(2021, Month.MAY, 9, 11, 6, 22));

        List<ProductHistory> historyList = new ArrayList<>();
        historyList.add(productHistory);

        product.setHistoryList(historyList);

        ProductResponseDTO productDTO = INSTANCE.productDTO(product, productHistory);

        assertThat( productDTO ).isNotNull();
        assertThat( productDTO.getArticle()).isEqualTo( "123ABC" );
        assertThat( productDTO.getName() ).isEqualTo( "test name" );
    }
}
