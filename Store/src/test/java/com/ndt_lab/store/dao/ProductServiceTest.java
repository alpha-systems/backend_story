package com.ndt_lab.store.dao;

import com.ndtLab.store.dao.ProductRepository;
import com.ndtLab.store.dto.ProductResponseDTO;
import com.ndtLab.store.dto.ProductsListResponseDTO;
import com.ndtLab.store.entity.Manufacturer;
import com.ndtLab.store.entity.Product;
import com.ndtLab.store.entity.ProductHistory;
import com.ndtLab.store.entity.Provider;
import com.ndtLab.store.mapper.ProductMapper;
import com.ndtLab.store.services.ProductServiceImpl;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mapstruct.factory.Mappers;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;
import org.mockito.InjectMocks;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.web.servlet.MockMvc;

import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.time.Month;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class ProductServiceTest {

    @InjectMocks
    private ProductServiceImpl productService;

    ProductMapper mapper = Mappers.getMapper(ProductMapper.class);

    @Mock
    ProductRepository productRepository;

    @Before
    public void setUp() {
        productService = new ProductServiceImpl(mapper, productRepository);
    }

    @Test
    public void findAllProducts() {
        Product product1 = createTestProduct(1L, "1Article");
        Product product2 = createTestProduct(2L, "2Article");

        BigDecimal price1 = BigDecimal.valueOf(1000);
        BigDecimal price2 = BigDecimal.valueOf(100);
        LocalDateTime endDate = LocalDateTime.of(2500, Month.JANUARY, 1, 0, 0, 0);

        ProductHistory productHistory1 = createTestProductHistory(1L, endDate, price1, product1);
        ProductHistory productHistory2 = createTestProductHistory(2L, endDate, price2, product2);

        when(productRepository.findAllActualHistoriesOfProducts()).thenReturn(Arrays.asList(product1, product2));
        List<ProductsListResponseDTO> actual = productService.findAllProducts();

        assertEquals(2, actual.size());
        assertEquals("1Article", actual.get(0).getArticle());
        assertEquals(price2, actual.get(1).getPrice());
    }

    @Test
    public void findHistoryOfProduct() {
        Product product1 = createTestProduct(1L, "1Article");
        LocalDateTime overDueDate = LocalDateTime.of(2022, Month.JANUARY, 1, 0, 0, 0);
        LocalDateTime endDate = LocalDateTime.of(2500, Month.JANUARY, 1, 0, 0, 0);
        BigDecimal price1 = BigDecimal.valueOf(1000);
        BigDecimal price2 = BigDecimal.valueOf(100);
        ProductHistory productHistory1 = createTestProductHistory(1L, overDueDate, price1, product1);
        ProductHistory productHistory2 = createTestProductHistory(2L, endDate, price2, product1);

        when(productRepository.findAllActualHistoryOfProduct(1L)).thenReturn(Optional.of(product1));

        ProductResponseDTO productResponseDTO = productService.findProduct(1L);

        assertEquals(product1.getArticle(), productResponseDTO.getArticle());
        assertEquals(price2, productResponseDTO.getPrice());
    }

    private Product createTestProduct(Long productId, String article) {
        Product product = new Product(productId, article, null);
        return product;
    }

    private ProductHistory createTestProductHistory(Long productHistoryId, LocalDateTime endDate, BigDecimal price, Product product) {
        Manufacturer manufacturer = new Manufacturer(1L, "test manufacturer");
        Provider provider = new Provider(1L, "test provider");
        BigDecimal discount = BigDecimal.valueOf(10);

        ProductHistory productHistory = new ProductHistory(productHistoryId, product, "test name",
                "test method", price, discount, false, manufacturer, provider,
                "test description", LocalDateTime.of(2021, Month.MAY, 9, 11, 6, 22),
                endDate, 1,
                LocalDateTime.of(2021, Month.MAY, 9, 11, 6, 22),
                1, LocalDateTime.of(2021, Month.MAY, 9, 11, 6, 22));

        List historyList = new ArrayList<>();
        historyList.add(productHistory);

        product.setHistoryList(historyList);
        return productHistory;
    }
}