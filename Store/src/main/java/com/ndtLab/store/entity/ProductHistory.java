package com.ndtLab.store.entity;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.hibernate.annotations.Type;

import javax.persistence.*;
import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.util.HashMap;
import java.util.Map;

@Entity
@Data
@NoArgsConstructor
@AllArgsConstructor
@Table(name = "sto_product_histories")
public class ProductHistory {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long productHistoryId;
    @ManyToOne(fetch = FetchType.LAZY)
    @JsonProperty(access = JsonProperty.Access.WRITE_ONLY)
    @JoinColumn(name = "product_id")
    private Product productId;
    @Column(nullable = false)
    private String name;
    private String ndtMethod;
    private BigDecimal price;
    private BigDecimal discount;
    @Column(nullable = false)
    private Boolean markedToOrder;
    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "manufacturer_id")
    private Manufacturer manufacturerId;
    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "provider_id")
    private Provider providerId;
    @Column(nullable = false)
    private String description;

//    @Type(type = "json")
//    @Column(columnDefinition = "jsonb", nullable = false)
//    private Map specifications = new HashMap<>();

    @Column(nullable = false)
    private LocalDateTime beginDate;
    private LocalDateTime endDate;
    @Column(nullable = false)
    private int naviCreateUser;
    @Column(nullable = false)
    private LocalDateTime naviCreateDate;
    private int naviUpdateUser;
    private LocalDateTime naviUpdateDate;

    @PrePersist
    protected void onCreate() {
        this.naviUpdateDate = LocalDateTime.now();
    }
}

