package com.ndtLab.store.dto;

import lombok.Data;

import java.math.BigDecimal;

@Data
public class ProductsListResponseDTO {
    private Long productId;
    private String name;
    private String article;
    private BigDecimal price;
    private BigDecimal discount;
}
