package com.ndtLab.store.dto;

import lombok.Data;

import java.math.BigDecimal;

@Data
public class ProductResponseDTO {
    private Long productId;
    private String name;
    private String article;
    private String ndtMethod;
    private BigDecimal price;
    private BigDecimal discount;
    private Boolean markedToOrder;
    private String description;
}
