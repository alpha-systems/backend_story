package com.ndtLab.store.dao;

import com.ndtLab.store.entity.Product;
import com.ndtLab.store.entity.ProductHistory;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface ProductRepository extends JpaRepository<Product, Long> {

    @Query("SELECT product, history FROM Product product LEFT JOIN FETCH product.historyList history " +
            "WHERE history.endDate = '2500-01-01 00:00:00.0'")
    List<Product> findAllActualHistoriesOfProducts();

    @Query("SELECT product, history FROM Product product LEFT JOIN FETCH product.historyList history " +
            "WHERE product.productId = ?1 and history.endDate = '2500-01-01 00:00:00.0'")
    Optional<Product> findAllActualHistoryOfProduct(Long id);
}