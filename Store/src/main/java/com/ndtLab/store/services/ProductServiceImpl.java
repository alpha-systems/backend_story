package com.ndtLab.store.services;

import com.ndtLab.store.dao.ProductRepository;
import com.ndtLab.store.dto.ProductResponseDTO;
import com.ndtLab.store.dto.ProductsListResponseDTO;
import com.ndtLab.store.entity.Product;
import com.ndtLab.store.exceptions.ProductNotFoundException;
import com.ndtLab.store.mapper.ProductMapper;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
@RequiredArgsConstructor
public class ProductServiceImpl implements ProductService {

    private final ProductMapper mapper;

    private final ProductRepository productRepository;

    @Override
    public List<ProductsListResponseDTO> findAllProducts() {
//        Iterable<Product> iterable = productRepository.findAllActualHistoriesOfProducts();
//        List<ProductsListResponseDTO> products = new ArrayList<>();
//        for (Product product : iterable) {
//            products.add(mapper.productsListDTO(product, product.getHistoryList().get(0)));
//        }
//        return products;

        return productRepository.findAllActualHistoriesOfProducts()
                .stream().map(product -> mapper.productsListDTO(product, product.getHistoryList().get(0))).collect(Collectors.toList());
    }

    @Override
    public ProductResponseDTO findProduct(Long id) {
        Optional<Product> product = productRepository.findAllActualHistoryOfProduct(id);

        if (product.isEmpty()) {
            throw new ProductNotFoundException("There is not product with ID = " +
                    id + " in Database");
        }
        return mapper.productDTO(product.get(), product.get().getHistoryList().get(0));
    }
}