package com.ndtLab.store.services;

import com.ndtLab.store.dto.ProductResponseDTO;
import com.ndtLab.store.dto.ProductsListResponseDTO;

import java.util.List;

public interface ProductService {

    List<ProductsListResponseDTO> findAllProducts();

    ProductResponseDTO findProduct(Long id);
}
