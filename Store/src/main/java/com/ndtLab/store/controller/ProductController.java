package com.ndtLab.store.controller;

import com.ndtLab.store.dto.ProductResponseDTO;
import com.ndtLab.store.dto.ProductsListResponseDTO;
import com.ndtLab.store.services.ProductService;

import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequiredArgsConstructor
@RequestMapping("api")
@CrossOrigin
public class ProductController {

    private final ProductService productService;

    @GetMapping("/products")
    List<ProductsListResponseDTO> getListProducts(){
        return productService.findAllProducts();
    }

    @GetMapping("/product/{id}")
    public ProductResponseDTO getProduct(@PathVariable Long id) {
        return productService.findProduct(id);
    }
}