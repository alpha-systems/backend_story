package com.ndtLab.store.mapper;

import com.ndtLab.store.dto.ProductResponseDTO;
import com.ndtLab.store.dto.ProductsListResponseDTO;
import com.ndtLab.store.entity.Product;
import com.ndtLab.store.entity.ProductHistory;
import org.mapstruct.*;
import org.mapstruct.factory.Mappers;

@Mapper
public interface ProductMapper {

    @Mapping(source = "product.productId", target = "productId")
    ProductResponseDTO productDTO(Product product, ProductHistory history);

    @Mapping(source = "product.productId", target = "productId")
    ProductsListResponseDTO productsListDTO(Product product, ProductHistory history);
}